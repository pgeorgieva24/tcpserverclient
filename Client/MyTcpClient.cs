﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Client
{
    class MyTcpClient
    {
        TcpClient _client;
        StreamReader streamReader;
        StreamWriter streamWriter;
        string IpAddress;
        int Port;

        public MyTcpClient(string ipAddress, int port)
        {
            _client = new TcpClient();
            //_client.Connect(ipAddress, port);
            IpAddress = ipAddress;
            Port = port;


        }

        public void HandleCommunication()
        {
            try
            {
                _client.Connect(IpAddress, Port);
                streamReader = new StreamReader(_client.GetStream());
                streamWriter = new StreamWriter(_client.GetStream());
                Thread thread = new Thread(new ThreadStart(ReceiveMessage));
                thread.Start();
                SendMessages();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception thrown while trying to connect to server: " + e.Message);
            }
        }

        void SendMessages()
        {
            try
            {
                string data = null;
                while (true)
                {
                    data = Console.ReadLine();
                    if (!string.IsNullOrWhiteSpace(data))
                    {
                        if (data.Equals("exit", StringComparison.OrdinalIgnoreCase))
                            break;
                        streamWriter.WriteLine(data);
                        streamWriter.Flush();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception in sending message: {0}", e.Message);
            }
            finally
            {
                _client.Close();
            }
        }

        void ReceiveMessage()
        {
            try
            {
                string data = null;
                while (true)
                {
                    data = streamReader.ReadLine();
                    if (data == null)
                        break;
                    Console.WriteLine(data);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception in receiving message: {0}", e.Message);
            }
            finally
            {
                _client.Close();
            }
        }
    }
}
