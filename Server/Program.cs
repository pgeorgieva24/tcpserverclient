﻿using System;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Multi-Threaded TCP Server Demo");
            Console.WriteLine("Waiting...");


            TcpServer server = new TcpServer(23);
            server.LoopClients();

            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }
    }
}
