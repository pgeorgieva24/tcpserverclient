﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Server
{
    class TcpServer
    {
        private TcpListener _server;
        bool isActive = false;

        List<TcpClient> listTcpClients = new List<TcpClient>();

        public TcpServer(int port)
        {
            _server = new TcpListener(IPAddress.Any, port);
        }

        public void LoopClients()
        {
            _server.Start();
            isActive = true;

            Task.Run(() => ListenForEscapeKeyToCloseListener());

            try
            {
                while (true)
                {
                    TcpClient client = _server.AcceptTcpClient();

                    listTcpClients.Add(client);

                    Thread t = new Thread(new ParameterizedThreadStart(HandleClient));
                    t.Start(client);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception thrown while waiting to accept client: {0}", e.Message);
                listTcpClients.ForEach(x => x.Close());
            }
        }

        private void HandleClient(object obj)
        {
            TcpClient tcpClient = (TcpClient)obj;

            var streamReader = new StreamReader(tcpClient.GetStream());
            var streamWriter = new StreamWriter(tcpClient.GetStream());

            streamWriter.WriteLine("Hi. You have successfuly made a connection to server. To leave write 'exit'.");
            streamWriter.Flush();

            var clientName = GetClientName(streamReader, streamWriter);

            Console.WriteLine(clientName + " just joined us");

            string data = null;
            while (isActive)
            {
                try
                {
                    if ((data = streamReader.ReadLine()) == null)
                        break;

                    string message = NameWithMessage(clientName, data);
                    Console.WriteLine(message);

                    if (listTcpClients.Count() == 1)
                    {
                        streamWriter.WriteLine("no one is in the chatroom");
                        streamWriter.Flush();
                        continue;
                    }

                    foreach (var client in listTcpClients.Except(new List<TcpClient> { tcpClient }))
                    {
                        var strWriter = new StreamWriter(client.GetStream());
                        strWriter.WriteLine(message);
                        strWriter.Flush();
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("exeption from handle client " + e.Message);
                    break;
                }
            }
            Console.WriteLine(clientName + " left the chat");
            listTcpClients.Remove(tcpClient);
        }

        string GetClientName(StreamReader streamReader, StreamWriter streamWriter)
        {
            string name = "";
            try
            {
                streamWriter.WriteLine("Name:");
                streamWriter.Flush();
                while (string.IsNullOrWhiteSpace(name = streamReader.ReadLine()))
                {
                    streamWriter.WriteLine("Name cannot be empty:");
                    streamWriter.Flush();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception thrown while trying to get client's name: " + e.Message);
            }
            return name;
        }

        string NameWithMessage(string name, string message)
        {
            return name + ": " + message;
        }

        void ListenForEscapeKeyToCloseListener()
        {
            try
            {
                while (true)
                {
                    var data = Console.ReadKey().Key;
                    if (data == ConsoleKey.Escape)
                    {
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception in listening for escape key message: {0}", e.Message);
            }
            finally
            {
                foreach (var client in listTcpClients)
                {
                    var strWriter = new StreamWriter(client.GetStream());
                    strWriter.WriteLine("Server was stopped manually. You cannot send messages right now. Press key to exit.");
                    strWriter.Flush();
                }
                _server.Stop();
                isActive = false;
            }
        }
    }
}